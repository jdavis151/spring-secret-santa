package thresh.springsecretsanta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecretSantaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecretSantaApplication.class, args);
	}

}
